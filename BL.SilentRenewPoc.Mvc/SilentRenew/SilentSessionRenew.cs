﻿using System;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityModel;
using IdentityModel.Client;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Notifications;
using Microsoft.Owin.Security.OpenIdConnect;

namespace BL.SilentRenewPoc.Mvc.SilentRenew
{
    public static class SilentSessionRenew
    {
        private delegate bool ValidateExpirationStrategy(DateTimeOffset currentUtc, DateTimeOffset issuedUtc,
            DateTimeOffset expiresUtc);


        private static readonly HttpClient HttpClient = new HttpClient();


        public static void Apply(
            CookieAuthenticationOptions cookieOptions,
            OpenIdConnectAuthenticationOptions oidcOptions)
        {
            if (cookieOptions == null)
            {
                throw new ArgumentNullException(nameof(cookieOptions));
            }

            if (oidcOptions == null)
            {
                throw new ArgumentNullException(nameof(oidcOptions));
            }

            // TODO: add id token update

            var oidcNotifications = Wrap(oidcOptions.Notifications);
            oidcNotifications.AuthorizationCodeReceived += OnAuthorizationCodeReceived(cookieOptions);
            oidcOptions.Notifications = oidcNotifications;

            var cookieAuthenticationProvider = Wrap(cookieOptions.Provider);
            cookieAuthenticationProvider.OnValidateIdentity += OnValidateIdentity(oidcOptions);
            cookieOptions.Provider = cookieAuthenticationProvider;
        }


        private static Func<CookieValidateIdentityContext, Task> OnValidateIdentity(
            OpenIdConnectAuthenticationOptions oidcOptions,
            ValidateExpirationStrategy validateExpirationStrategy = null)
        {
            var validateExpiration = validateExpirationStrategy ?? ValidateExpiration;

            return async context =>
            {
                var identity = context.Identity;

                var idTokenClaim = identity.FindFirst(OpenIdConnectParameterNames.IdToken);
                var accessTokenClaim = identity.FindFirst(OpenIdConnectParameterNames.AccessToken);
                var expiresInClaim = identity.FindFirst(OpenIdConnectParameterNames.ExpiresIn);
                var refreshTokenClaim = identity.FindFirst(OpenIdConnectParameterNames.RefreshToken);
                if (new[] {idTokenClaim, accessTokenClaim, expiresInClaim, refreshTokenClaim}.Any(c => c == null))
                {
                    return;
                }

                var maybeIssuedUtc = context.Properties.IssuedUtc;
                if (!maybeIssuedUtc.HasValue)
                {
                    return;
                }

                var issuedUtc = maybeIssuedUtc.Value;
                var expiresIn = int.Parse(expiresInClaim.Value);
                var expiresUtc = issuedUtc.AddSeconds(expiresIn);
                // TODO: abstract DateTimeOffset.UtcNow?
                var currentUtc = context.Options.SystemClock?.UtcNow ?? DateTimeOffset.UtcNow;

                if (!validateExpiration(currentUtc, issuedUtc, expiresUtc))
                {
                    return;
                }

                var refreshToken = refreshTokenClaim.Value;
                var tokenResponse = await HttpClient.RequestRefreshTokenAsync(
                    new RefreshTokenRequest
                    {
                        // TODO: from cfg
                        Address = $"{oidcOptions.Authority}/connect/token",
                        ClientId = oidcOptions.ClientId,
                        ClientSecret = oidcOptions.ClientSecret,
                        RefreshToken = refreshToken,
                        // TODO: scope
                    });

                var handled = HandleFailedTokenResponse(tokenResponse, () =>
                {
                    context.RejectIdentity();
                    context.OwinContext.Authentication.SignOut(context.Options.AuthenticationType);
                });
                if (handled)
                {
                    return;
                }

                // TODO: update Identity from new id_token

                identity.RemoveClaim(idTokenClaim);
                identity.AddClaim(new Claim(OpenIdConnectParameterNames.IdToken, tokenResponse.IdentityToken));

                identity.RemoveClaim(accessTokenClaim);
                identity.AddClaim(new Claim(OpenIdConnectParameterNames.AccessToken, tokenResponse.AccessToken));

                identity.RemoveClaim(expiresInClaim);
                identity.AddClaim(new Claim(OpenIdConnectParameterNames.ExpiresIn, tokenResponse.ExpiresIn.ToString()));

                identity.RemoveClaim(refreshTokenClaim);
                identity.AddClaim(new Claim(OpenIdConnectParameterNames.RefreshToken, tokenResponse.RefreshToken));

                // setting it to null so that it is refreshed by the cookie middleware
                context.Properties.IssuedUtc = null;
                context.Properties.ExpiresUtc = null;
                context.OwinContext.Authentication.SignIn(context.Properties, identity);
            };
        }

        // the OpenIdConnectAuthentication middleware does not
        // automatically exchange the authorization code
        // for a token result, so we do it here.
        private static Func<AuthorizationCodeReceivedNotification, Task> OnAuthorizationCodeReceived(
            CookieAuthenticationOptions cookieOptions)
        {
            return async notification =>
            {
                var tokenResponse = await HttpClient.RequestAuthorizationCodeTokenAsync(
                    new AuthorizationCodeTokenRequest
                    {
                        // TODO: from cfg
                        Address = $"{notification.Options.Authority}/connect/token",
                        ClientId = notification.Options.ClientId,
                        ClientSecret = notification.Options.ClientSecret,
                        RedirectUri = notification.RedirectUri,
                        Code = notification.Code,
                        // TODO: scope
                    });

                var handled = HandleFailedTokenResponse(tokenResponse, () =>
                {
                    var authenticationManager = notification.OwinContext.Authentication;
                    authenticationManager.SignOut(cookieOptions.AuthenticationType);
                });
                if (handled)
                {
                    return;
                }

                // TODO: update Identity from new id_token

                var identity = notification.AuthenticationTicket.Identity;
                identity.AddClaims(new[]
                {
                    new Claim(OpenIdConnectParameterNames.IdToken, tokenResponse.IdentityToken),
                    new Claim(OpenIdConnectParameterNames.RefreshToken, tokenResponse.RefreshToken),
                    new Claim(OpenIdConnectParameterNames.AccessToken, tokenResponse.AccessToken),
                    new Claim(OpenIdConnectParameterNames.ExpiresIn, tokenResponse.ExpiresIn.ToString()),
                });
            };
        }

        private static bool HandleFailedTokenResponse(
            TokenResponse tokenResponse,
            Action handleInvalidToken)
        {
            switch (tokenResponse.ErrorType)
            {
                case ResponseErrorType.None:
                    // same as tokenResponse.IsError == false
                    return false;
                case ResponseErrorType.Protocol:
                    var tokenInvalid = tokenResponse.Error == OidcConstants.TokenErrors.InvalidGrant;
                    if (!tokenInvalid)
                    {
                        throw new OpenIdConnectProtocolException(
                            $"{tokenResponse.Error}. {tokenResponse.ErrorDescription}",
                            tokenResponse.Exception);
                    }

                    handleInvalidToken();

                    return true;
                case ResponseErrorType.Http:
                case ResponseErrorType.Exception:
                case ResponseErrorType.PolicyViolation:
                    throw new InvalidOperationException(
                        $"{tokenResponse.ErrorType.ToString()}. {tokenResponse.Error}. ${tokenResponse.ErrorDescription}",
                        tokenResponse.Exception);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static CookieAuthenticationProvider Wrap(
            ICookieAuthenticationProvider currentCookieAuthenticationProvider)
        {
            var wrappedCookieAuthenticationProvider = new CookieAuthenticationProvider();
            if (currentCookieAuthenticationProvider == null)
            {
                return wrappedCookieAuthenticationProvider;
            }

            wrappedCookieAuthenticationProvider.OnValidateIdentity +=
                currentCookieAuthenticationProvider.ValidateIdentity;
            wrappedCookieAuthenticationProvider.OnResponseSignIn +=
                currentCookieAuthenticationProvider.ResponseSignIn;
            wrappedCookieAuthenticationProvider.OnResponseSignedIn +=
                currentCookieAuthenticationProvider.ResponseSignedIn;
            wrappedCookieAuthenticationProvider.OnResponseSignOut +=
                currentCookieAuthenticationProvider.ResponseSignOut;
            wrappedCookieAuthenticationProvider.OnApplyRedirect +=
                currentCookieAuthenticationProvider.ApplyRedirect;
            wrappedCookieAuthenticationProvider.OnException +=
                currentCookieAuthenticationProvider.Exception;

            return wrappedCookieAuthenticationProvider;
        }

        private static OpenIdConnectAuthenticationNotifications Wrap(
            OpenIdConnectAuthenticationNotifications oidcNotifications)
        {
            var wrappedOidcNotifications = oidcNotifications ?? new OpenIdConnectAuthenticationNotifications();

            return wrappedOidcNotifications;
        }

        private static bool ValidateExpiration(DateTimeOffset currentUtc, DateTimeOffset issuedUtc,
            DateTimeOffset expiresUtc)
        {
            // since our cookie lifetime is big,
            // check if we're more than halfway of the token lifetime
            var timeElapsed = currentUtc.Subtract(issuedUtc);
            var timeRemaining = expiresUtc.Subtract(currentUtc);

            return timeElapsed > timeRemaining;
        }
    }
}