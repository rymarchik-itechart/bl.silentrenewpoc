﻿using System;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using Owin;
using BL.SilentRenewPoc.Mvc.SilentRenew;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;

[assembly: OwinStartup(typeof(BL.SilentRenewPoc.Mvc.Startup))]

namespace BL.SilentRenewPoc.Mvc
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            const string signInAsAuthenticationType = CookieAuthenticationDefaults.AuthenticationType;

            var cookieAuthenticationOptions = new CookieAuthenticationOptions
            {
                AuthenticationType = signInAsAuthenticationType,

                // important for proper cookie refresh
                SlidingExpiration = true,
                ExpireTimeSpan = new TimeSpan(365, 0, 0, 0),
            };
            app.UseCookieAuthentication(cookieAuthenticationOptions);

            var openIdConnectAuthenticationOptions = new OpenIdConnectAuthenticationOptions
            {
                Authority = "https://localhost:44364",

                ClientId = "mvc",
                ClientSecret = "secret",
                Scope = "openid offline_access profile",
                ResponseType = "code id_token",
                RedirectUri = "https://localhost:44313/signin-oidc",
                SignInAsAuthenticationType = signInAsAuthenticationType,
                // we will use 1 year cookie expire timespan
                // and then validate access_token lifetime manually
                UseTokenLifetime = false,
            };
            app.UseOpenIdConnectAuthentication(openIdConnectAuthenticationOptions);

            SilentSessionRenew.Apply(cookieAuthenticationOptions, openIdConnectAuthenticationOptions);
        }
    }
}