﻿using System.Web;
using System.Web.Mvc;

namespace BL.SilentRenewPoc.Mvc.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult Secured()
        {
            return View();
        }

        public ActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index");
            }

            HttpContext.Request.GetOwinContext().Authentication.Challenge();
            return new EmptyResult();
        }

        public ActionResult Logout()
        {
            HttpContext.Request.GetOwinContext().Authentication.SignOut();
            return new EmptyResult();
        }
    }
}