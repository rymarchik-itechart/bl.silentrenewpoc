﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;

namespace BL.SilentRenewPoc.IdentityServer4
{
    public static class Config
    {
        public static List<TestUser> GetUsers()
        {
            return new List<TestUser>
            {
                new TestUser
                {
                    SubjectId = "1",
                    Username = "alice",
                    Password = "alice",

                    Claims = new[]
                    {
                        new Claim("name", "Alice"),
                    }
                },
                new TestUser
                {
                    SubjectId = "2",
                    Username = "bob",
                    Password = "bob",

                    Claims = new[]
                    {
                        new Claim("name", "Bob"),
                    }
                }
            };
        }

        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };
        }

        public static IEnumerable<ApiResource> GetApis()
        {
            return new List<ApiResource>
            {
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                // OpenID Connect hybrid flow client (MVC)
                new Client
                {
                    ClientId = "mvc",
                    ClientName = "MVC Client",
                    AllowedGrantTypes = GrantTypes.Hybrid,

                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },

                    RedirectUris = {"https://localhost:44313/signin-oidc"},
                    PostLogoutRedirectUris = {"https://localhost:44313/signout-callback-oidc"},

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                    },

                    // to enable refresh_token
                    AllowOfflineAccess = true,

                    // as we are using pretty short tokens expiration (15 min) it's better
                    // to use reusable sliding refresh token so we won't spam the database
                    RefreshTokenExpiration = TokenExpiration.Sliding,
                    RefreshTokenUsage = TokenUsage.ReUse,

                    // AbsoluteRefreshTokenLifetime and SlidingRefreshTokenLifetime should not be greater that cookie expiration defined in client
                    AbsoluteRefreshTokenLifetime = (int) new TimeSpan(365, 0, 0, 0).TotalSeconds,
                    SlidingRefreshTokenLifetime = (int) new TimeSpan(365, 0, 0, 0).TotalSeconds,
                    
                    // tokens lifetime should be less then security stamp validation interval in our IS4
                    AccessTokenLifetime = (int) TimeSpan.FromMinutes(15).TotalSeconds,
                    IdentityTokenLifetime = (int) TimeSpan.FromMinutes(15).TotalSeconds,
                }
            };
        }
    }
}